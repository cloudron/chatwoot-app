[0.1.0]
* initial version with Chatwoot 1.21.1

[0.2.0]
* Fix image uploads

[0.3.0]
* Fix transactional emails

[1.0.0]
* First stable package release based on Chatwoot 1.21.1

[1.1.0]
* Update Chatwoot to 1.22.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v1.22.0)
* Chatwoot command bar for improved productivity
* Ability to define and add custom attributes to conversations/contacts
* Ability to add notes to contact on the CRM page
* Ability to configure Dialogflow integration across all channel types except email
* Ability to rearrange the conversation sidebar
* Ability to specify subject to outgoing emails

[1.1.1]
* Update Chatwoot to 1.22.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v1.22.1)
* Fix the issue of agent messages leaking into unverified contact sessions having the same contact email
* Added indication in UI for unverified contact sessions

[1.2.0]
* Update Chatwoot to 2.0.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.0.0)
* IMAP and SMTP support for email channels
* Dedicated View for conversation mentions
* Advanced conversation filters
* Advanced contact filters
* Template support for 360 dialog WhatsApp channels
* List and Checkbox support for custom attributes
* View conversation summary in notification emails
* OpenAPI compliant swagger docs
* Numerous bug fixes and enhancements

[1.2.1]
* Update to base image v3.1.0
* Fix .env file loading

[1.2.2]
* Refactor env file so it can be sourced to run rails commands

[1.2.3]
* Update Chatwoot to 2.0.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.0.1)
* Fixed memory leak with email parsing
* Attachment Upload improvements

[1.2.4]
* Update Chatwoot to 2.0.2
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.0.2)
* Issues with conversation assignment notifications

[1.2.5]
* Update Chatwoot to 2.1.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.1.0)
* Ability to disable messages in resolved Live chat conversations
* Support more filetypes as message attachments
* Ability to disable tweet conversation in the Twitter inbox
* Improved email confirmation flow for user accounts
* Improved email parsing
* Improved background job processing
* New Language: Slovak(sk)

[1.2.6]
* Update Chatwoot to 2.1.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.1.1)
* Fixing the delayed assignment notifications reported in some instances

[1.3.0]
* Update Chatwoot to 2.2.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.2.0)
* Automations ( Beta )
* Custom Views for conversations page
* Custom Filters for contacts page
* Email Signatures
* Support Bandwidth as a provider for SMS channel
* Ability to configure active storage direct uploads
* Ability to send multiple attachments from the agent dashboard
* Super Admin dashboard enhancements
* Support for interactive messages in Whatsapp channel ( 360 Dialog )
* Ability to group report data by weekly, monthly and yearly filters
* Ability to toggle conversation continuity via email for website channel
* Resolve action for Dialogflow integration
* SSL Verify mode and email encryption settings for SMTP mailboxes
* Vue-router for widget route management
* Numerous bug fixes and enhancements

[1.3.1]
* Update Chatwoot to 2.2.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.2.1)
* Fix for email messages without body
* Reporting improvements to consider timezone
* Fix for email signature being sent until it is unset in the editor
* Fix for nokogiri security vulnerability

[1.4.0]
* Update Chatwoot to 2.3.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.3.0)
* Timezone support in reports
* Ability for contacts to end conversations on chatwidget
* Ability to record and send voice notes from the agent dashboard
* Support for Hcaptcha in public forms
* Support for Instagram story mentions
* API to bulk update conversations
* Agent filter in CSAT reports
* Ability to configure 24-hour slots in business hours
* Support for MMS in Bandwidth SMS channel
* Ability to pass specific account ids when generating SSO URLs
* Display trends in reporting metrics
* Numerous bug fixes and enhancements

[1.4.1]
* Update Chatwoot to 2.3.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.3.1)
* Improves performance of agent endpoints

[1.4.2]
* Update Chatwoot to 2.3.2
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.3.2)
* fix for the invalid contact resolved conversation activity message
* fix for file copy-paste issue
* fix for the pre-chat form being rendered for identified contacts
* fix for auto-scroll not working in the dashboard
* sentry error fixes and improvements

[1.4.3]
* Update Chatwoot to 2.4.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.4.0)
* Notification view with real-time indicators
* Dark mode for the website widget
* Reports business hours
* IMAP SMTP improvements to support more providers
* Rich editor and cc support for new conversations
* Automation improvements
* Round robin assignment support for teams
* SDK methods for the pop-out chat widget
* ee edition docker images
* Numerous bug fixes and enhancements

[1.4.4]
* Update Chatwoot to 2.4.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.4.1)
* Fixing the accidentally merged conditions for open all day and closed all day.

[1.5.0]
* Update Chatwoot to 2.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.5.0)

[1.6.0]
* Update Chatwoot to 2.6.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.6.0)

[1.7.0]
* Update Chatwoot to 2.7.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.7.0)
* Support for Whatsapp Cloud API
* Ability to create dashboard apps from UI
* New Chatwoot CLI tool
* Support for Twilio Messaging Services
* Support HMAC enforcement in API channel
* Enhanced contact identification and merge logic
* APIs for custom sort
* API for updating contact avatar
* Numerous bug fixes and enhancements

[1.8.0]
* Update Chatwoot to 2.8.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.8.0)
* Email layout with improved email rendering
* Context Menu for conversations
* Chat Widget Builder
* CRUD APIs for macros
* Agent Capacity Config (enterprise)
* Ability to disable Gravatars
* Support for Elastic APM
* New Langauge: Thai
* Numerous API enhancements and bug fixes

[1.8.1]
* Update Chatwoot to 2.8.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.8.1)

[1.9.0]
* Update Chatwoot to 2.9.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.9.0)
* Help Center (Beta)
* Email conversation continuity for API channels
* Ability for Dashboard App Frames to query the information
* Improved blocking & throttling of abusive requests
* Automations supporting case insensitive filters
* Numerous API enhancements and bug fixes

[1.9.1]
* Update Chatwoot to 2.9.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.9.1)
* Minor help center related fixes

[1.10.0]
* Update Chatwoot to 2.10.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.10.0)
* Support for draft replies
* Ability to customise keymapping for the send reply button
* Ability to create canned responses from the dashboard
* Improvements to the Help Center module
* Performance improvements

[1.11.0]
* Update Chatwoot to 2.11.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.11.0)
* Macros
* Ability to search in help centre public portals
* Support for location messages in the WhatsApp channel
* Support for custom attributes in automation
* Ability to paste files/images from the Clipboard
* Realtime update for conversation attributes on agent dashboard
* Redis connection performance improvements
* Numerous other enhancements and bug fixes

[1.12.0]
* Add support for custom domains in helpdesk feature

[1.13.0]
* Update Chatwoot to 2.12.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.12.0)
* Message Delivery and read status for Whatsapp Channel
* Ability to view unattended conversations
* Ability to create and configure agent bots from UI
* Ability to mark a conversation as unread
* Ability to lock conversations to a single thread for supported channels
* Ability to use postfix as the mailer in self-hosted installations
* Ability to search in emoji selector
* Allow wildcard URLs in campaigns
* Allow users to disable marking offline automatically
* Numerous bug fixes and enhancements
* New languages: Latvian

[1.13.1]
* Update Chatwoot to 2.12.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.12.1)
* Fix the issue with WhatsApp voice recorder recording in WebM format.
* Fix the issue with WhatsApp's unread message count
* Add an option to select the audio alert tone
* Add an option to set the alert for every 30s unless the conversation is read

[1.14.0]
* Update Chatwoot to 2.13.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.13.0)
* Ability to do video calls on Chatwoot through the Dyte Integration
* Native support for Microsoft as a provider in Email Inboxes
* Ability to use template variables while messaging
* Ability to send longer audio messages on Whatsapp Channel
* Support for incoming location messages in Telegram Channel
* Numerous bug fixes and enhancements

[1.14.1]
* Update Chatwoot to 2.13.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.13.1)

[1.15.0]
* Update Chatwoot to 2.14.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.14.0)
* Conversation Participants
* Support for authentication via Google Oauth
* Google Translate Integration
* Image support in help centre articles
* UI for selecting Template variables
* Support contact attachments in Whatsapp Channel
* Support sending multiple attachments in Whatsapp Channel
* Ability to impersonate a user from Super Admin
* Ability to update Whatsapp Channel API key from UI
* Instance health status in Super Admin
* New Languages : Icelandic(is)
* Numerous bug fixes and enhancements

[1.15.1]
* Update to base image version 4

[1.16.0]
* Update Chatwoot to 2.15.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.15.0)
* Conversation Heatmap reports
* New Search experience
* RTL language support in Chatwoot Dashboard
* Audit Log APIs (EE edition)
* Support Rich messages in pre-chat form
* Support for WhatsApp messages from older Brazilian phone numbers
* Push notification support for safari
* Integration with Logrocket
* Numerous bug fixes and enhancements

[1.16.1]
* Migrate `/app/data/env` to `/app/data/env.sh`

[1.16.2]
* Make it easier to run bundle rails console

[1.17.0]
* Update Chatwoot to 2.16.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.16.0)
* Context Menu for messages ( while right-clicking )
* Ability for admins to edit agent availability status
* Ability to link to a specific message
* Support for message-updated events in the telegram channel
* Ability to download heatmap reports
* support routing in the email channel using ex-original-to in the email header
* Additional automation filters ( email and phone number based )

[1.17.1]
* Update Chatwoot to 2.16.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.16.1)
* Fixes the issue with the UI when name of the customer is not present.

[1.17.2]
* Update Chatwoot to 2.17.1
* Update Ruby to 3.2.2
* Requires Redis 7 and Cloudron 7.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.17.0)
* OpenAI integration : ( Reply suggestions, summarization, and ability to improve drafts )
* Priority field for conversations
* Ability to sort conversations
* Instagram story replies show the original story
* SLA APIs ( Enterprise Edition )
* Improved email fetching for IMAP inboxes
* Support template variables in channel greeting messages
* Ability to create uncategorized help center articles
* Automatic reconnects in the background
* Numerous bug fixes and performance enhancements
* New Languages: Hebrew
* Fixes issues with avatars in docker images
* Fixes issues with pt-br translations

[1.18.0]
* Update Chatwoot to 2.17.1
* Update Ruby to 3.2.2
* Requires Redis 7 and Cloudron 7.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.17.0)
* OpenAI integration : ( Reply suggestions, summarization, and ability to improve drafts )
* Priority field for conversations
* Ability to sort conversations
* Instagram story replies show the original story
* SLA APIs ( Enterprise Edition )
* Improved email fetching for IMAP inboxes
* Support template variables in channel greeting messages
* Ability to create uncategorized help center articles
* Automatic reconnects in the background
* Numerous bug fixes and performance enhancements
* New Languages: Hebrew
* Fixes issues with avatars in docker images
* Fixes issues with pt-br translations

[1.18.1]
* Update Chatwoot to 2.18.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.18.0)
* New attachment view for conversations
* Ability to export contacts
* Support for interactive messages in WhatsApp ( button and list )
* Ability to update bot/user avatars from Super Admin Dashboard
* Audit logs for sign_in, sign_out, team events (EE)
* Support for configuring subdomains in widget SDK
* Enhanced filters for CSAT responses
* Support for super script tag in help center articles
* Numerous bug fixes and enhancements

[1.18.2]
* Add missing libvips

[1.19.0]
* Update Chatwoot to 3.0.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/3.0.0)
* Dark mode

[1.20.0]
* Update Chatwoot to 3.1.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.1.0)
* Enhanced Knowledge Portal Integration with Chatwidget
* Advanced Email Message Signatures Supporting Image Attachments
* Optimized Contact Import Functionality
* Upgraded Slack Integration to Support Attachments
* Comprehensive Bug Resolutions and Performance Enhancements

[1.20.1]
* Update Chatwoot to 3.1.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.1.1)
* Fixes issues with filters involving labels
* Fixes filtering of articles in Chatwidget
* Fixes duplicate signature issue

[1.21.0]
* Update Chatwoot to 3.2.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.2.0)
* Ability to reply to a specific message in the agent dashboard
* Ability to send attachments while creating outbound conversations
* Delivery status updates for Twilio and live chat channels
* Support for link unfurling in Slack integration
* Dark theme support for knowledge base portals
* Ability to search contacts by company name

[1.22.0]
* Update Chatwoot to 3.3.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.3.0)
* Delivery reports for messages across all channels
* Reply to option for supported channels
* Helpcenter enhancements and redesign
* Ability to insert helpcenter articles as messages
* Ability to disable label suggestions with openAi integration
* Numerous performance improvements and bug fixes

[1.22.1]
* Update Chatwoot to 3.3.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.3.1)
* Fixes the issue with the message pane disappearing when OpenAI integration isn't enabled

[1.23.0]
* Update Chatwoot to 3.4.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.4.0)
* Ability to manage premium self-hosted plans from super admin pannel
* Ability to manage white label settings from super admin pannel ( enterprise )
* Support for stickers and outgoing attachments in line channel
* Support for more sort options in conversation dashboard
* Support for custom attributes in message variables
* Delivery reports for Facebook channel
* Support for Authentication in case of Twilio media messages
* Multiple performance improvements and bug fixes

[1.24.0]
* Update Chatwoot to 3.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.5.0)
* Improved configuration screen for Whitelabel settings
* UI for unsupported message types in Instagram/Facebook Channel
* Custom attributes in the suggestion for variables
* Bugfixes and improvements

[1.24.1]
* Update Chatwoot to 3.5.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.5.1)
* Fixes the issue with the telegram channel, where messages containing special characters were not getting delivered.

[1.24.2]
* Update Chatwoot to 3.5.2
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.5.2)
* Fixes issue with super admin token when making API calls - #8719
* Fixes issues with Email Channel bandwidth exceeded issues - #6082
* Switch to HTML parse mode in telegram channel to address the message sending issues
* Numerous other fixes and improvements

[1.25.0]
* Update Chatwoot to 3.6.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.6.0)
* Support for regex-validated fields in pre-chat form
* Improved email loading for email channel when using IMAP
* Markdown support for the telegram channel
* Ability to update multiple files in Facebook & Instagram channels
* Custom branding support for the help centre
* Numerous performance improvements and bug fixes

[1.26.0]
* Update Chatwoot to 3.7.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.7.0)
* SLA for conversations (enterprise) - (alpha)
* Inbox view for notifications - (alpha)
* Reports for conversations handled by bots
* Ability to block contacts
* Conversation image improvements for agents ( zoom, preview inline images, etc )
* Export contact improvements
* Numerous bug fixes and enhancements

[1.27.0]
* Update Chatwoot to 3.8.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.8.0)
* SLAs for conversations ( enterprise )
* Embedding based search for Helpcenter ( enterprise - alpha )
* Lock to Single Thread config for meta inbox
* Improved email fetching for IMAP inboxes
* API to configure Chat widget into a help center
* Numerous bug fixes and enhancements

[1.28.0]
* Update Chatwoot to 3.9.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.9.0)
* Performance improvements for reports (7x better load times on larger accounts)
* Improved performance for conversation filters
* Ability to export contacts with filters applied
* Ability to snooze conversations in bulk actions
* Support file uploads in the telegram channel
* Support for loom videos in help centre articles
* Numerous bug fixes and enhancements

[1.29.0]
* Update Chatwoot to 3.10.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.10.0)

[1.29.1]
* Update Chatwoot to 3.10.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.10.1)

[1.29.2]
* Update Chatwoot to 3.10.2
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.10.2)
* Improved video rendering in the widget
* Additional fixes for notification payload

[1.30.0]
* Update Chatwoot to 3.11.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.11.0)
* Ability to unassign teams using automation
* Add video message viewing in Chatwidget
* Improved reconnect logic for automated reconnections
* API frameworks for upcoming roles & permissions feature
* Upgraded ruby to 3.3.3 for performance improvements
* Numerous bug fixes and enhancements

[1.30.1]
* Update Chatwoot to 3.11.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.11.1)
* This hotfix release fixes the conversation order when real-time events are delayed or missed.

[1.31.0]
* Update Chatwoot to 3.12.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.12.0)
* Design updates for various setting pages like integrations, canned responses, teams, etc
* Captain integration (alpha)
* Support for shared instagram reels in the messenger channel
* Changes for vue3 upgrade
* Numerous bug fixes and enhancements

[1.31.1]
* Update Chatwoot to 3.13.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.13.0)
* Custom Roles (alpha) [EE]
* Admin page redesigns for macros, automation, inbox management, etc
* Support for copy-pasting images in help center articles
* Update GPT model for open AI integration to 4o-mini
* Numerous bug fixes and enhancements

[1.32.0]
* Update Chatwoot to 3.14.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.14.0)
* Vite + vue 3 : Upgrades to our frontend stack and build pipelines
* Auto populate country code in phone number field based on browser timezone
* Numerous bug fixes and improvements

[1.32.1]
* Update Chatwoot to 3.14.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.14.1)
* Fixes blank screen-related issues after logout

[1.33.0]
* Update Chatwoot to 3.15.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.15.0)
* ip-lookup database lazy loading for all installation environments
* New sidebar for Chatwoot ( v4 )
* New design for HelpCenter portals ( v4 )
* New design for campaigns page ( v4 )

[1.34.0]
* Update chatwoot to 3.16.0
* [Full Changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.16.0)
* Design update for report pages
* Design update for contact pages
* Updated audio notification settings with new sounds
* Ability to lock conversations to single thread in API channels
* Captain copilot integration to sidebar (alpha)
* Ability to create conversation directly from email / phone number
* Add support for Arcade videos on articles
* Other Components and Design changes in preparation for v4
* Numerous bug fixes and enhancements

[1.34.1]
* Update rbenv to 1.3.1
* [Full Changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.16.0)
* Add `/usr/etc/rbenv.d` to hooks path by [@&#8203;tomschr](https://github.com/tomschr) in https://github.com/rbenv/rbenv/pull/1587
* Use `readarray` in bash v4+ to avoid rbenv init hanging by [@&#8203;mislav](https://github.com/mislav) in https://github.com/rbenv/rbenv/pull/1604
* Add instructions for Fedora Linux installation by [@&#8203;nethad](https://github.com/nethad) in https://github.com/rbenv/rbenv/pull/1583
* Skip BW01 and BW02 error messages during tests by [@&#8203;mikelolasagasti](https://github.com/mikelolasagasti) in https://github.com/rbenv/rbenv/pull/1600
* Bump mislav/bump-homebrew-formula-action from 3.1 to 3.2 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/rbenv/rbenv/pull/1598
* [@&#8203;nethad](https://github.com/nethad) made their first contribution in https://github.com/rbenv/rbenv/pull/1583
* [@&#8203;tomschr](https://github.com/tomschr) made their first contribution in https://github.com/rbenv/rbenv/pull/1587
* [@&#8203;mikelolasagasti](https://github.com/mikelolasagasti) made their first contribution in https://github.com/rbenv/rbenv/pull/1600

[1.34.2]
* Update rbenv to 1.3.2
* [Full Changelog](https://github.com/chatwoot/chatwoot/releases/tag/v3.16.0)
* Fix traversing PATH for bash < 4.4 by [@&#8203;mislav](https://github.com/mislav) in https://github.com/rbenv/rbenv/pull/1606
* [@&#8203;BiggerNoise](https://github.com/BiggerNoise) for helping me [debug rbenv init](https://github.com/rbenv/rbenv/issues/1593#issuecomment-2569402956)
* [@&#8203;Tobias-pwnr](https://github.com/Tobias-pwnr) for reporting a [bug with bash < 4.4](https://github.com/rbenv/rbenv/pull/1604#issuecomment-2576749563)
* [@&#8203;pnghai](https://github.com/pnghai) for the proposed fix [#&#8203;1605](https://github.com/rbenv/rbenv/issues/1605)

[1.35.0]
* checklist added to manifest

[1.36.0]
* Update chatwoot to 4.0.2
* [Full Changelog](https://github.com/chatwoot/chatwoot/releases/tag/v4.0.2)
* **Captain Assistant**: A customer-facing AI that provides real-time, context-aware responses.
* **Captain Co-pilot**: Helps agents with response preparation, translations, data retrieval, and more.
* **Captain FAQs**: Detects common unanswered questions to grow your knowledge base.
* **Captain Memories**: Logs key memories from conversations into the CRM for personalized, proactive support.
* **Inbox View**: A whole new way to keep up with notifications and conversations relevant to you in Chatwoot without distractions.
* **Better Email Previews**: Improved formatting for faster response times.
* **Redesigned Contacts Section**: Cleaner layout and intuitive filtering for managing customer details.

[1.36.1]
* Update chatwoot to 4.0.3
* [Full Changelog](https://github.com/chatwoot/chatwoot/releases/tag/v4.0.3)
* Fixed issues with new conversation form for whatsapp templates
* Fixed slack file upload related issues and support for latest file API changes
* Numerous other fixes and improvements


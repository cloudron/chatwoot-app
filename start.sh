#!/bin/bash

set -eu

mkdir -p /run/chatwoot/

echo "=> Starting chatwoot"

rm -rf /tmp/pids/server.pid /tmp/cache/* # Remove a potentially pre-existing server.pid for Rails

if [[ ! -f "/app/data/env.sh" ]]; then
    echo "=> Creating .env file at /app/data/env and Generating SECRET_KEY_BASE"
    secret_key_base=$(pwgen -snA 128 1)
    sed -e "s/SECRET_KEY_BASE=.*/SECRET_KEY_BASE=${secret_key_base}/" /app/pkg/env.sh.template > /app/data/env.sh
fi

echo "=> Sourcing /app/data/env.sh"
source /app/data/env.sh

cat > /run/chatwoot/env.sh <<EOF
export FRONTEND_URL="${CLOUDRON_APP_ORIGIN}"
export MAILER_SENDER_EMAIL="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Chatwoot} <${CLOUDRON_MAIL_FROM}>"
export SMTP_ADDRESS="${CLOUDRON_MAIL_SMTP_SERVER}"
export SMTP_USERNAME="${CLOUDRON_MAIL_SMTP_USERNAME}"
export SMTP_PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"
export SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}
export REDIS_URL="redis://${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}"
export REDIS_PASSWORD="${CLOUDRON_REDIS_PASSWORD}"
export POSTGRES_DATABASE="${CLOUDRON_POSTGRESQL_DATABASE}"
export POSTGRES_USERNAME="${CLOUDRON_POSTGRESQL_USERNAME}"
export POSTGRES_PASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"
export POSTGRES_HOST="${CLOUDRON_POSTGRESQL_HOST}"
export POSTGRES_PORT="${CLOUDRON_POSTGRESQL_PORT}"
EOF

source /run/chatwoot/env.sh # create a separate file, so it's easier for user to run rails console commands that require db

if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "=> Initializing db"
    bundle exec rails db:chatwoot_prepare RAILS_ENV=production
    touch /app/data/.dbsetup
else
    echo "=> Upgrading existing db"
    bundle exec rails db:chatwoot_prepare RAILS_ENV=production
fi

echo "=> Ensure directories"
mkdir -p /app/data/storage

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i chatwoot

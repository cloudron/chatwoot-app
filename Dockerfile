FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt-get update && \
    apt install -y libvips && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# install rbenv since we need specific ruby (https://github.com/rbenv/rbenv/releases)
# renovate: datasource=github-releases depName=rbenv/rbenv versioning=semver extractVersion=^v(?<version>.+)$
ARG RBENV_VERSION=1.3.2

# https://github.com/rbenv/ruby-build/releases
# renovate: datasource=github-releases depName=rbenv/rbenv-build versioning=regex:^(?<major>\d{4})(?<minor>\d{2})(?<patch>\d{2})\.?(?<build>.+)?$ extractVersion=^v(?<version>.+)$
ARG RUBYBUILD_VERSION=20240709.1
RUN mkdir -p /usr/local/rbenv && curl -LSs "https://github.com/rbenv/rbenv/archive/refs/tags/v${RBENV_VERSION}.tar.gz" | tar -xz -C /usr/local/rbenv --strip-components 1 -f -
ENV PATH=/usr/local/rbenv/bin:$PATH
RUN mkdir -p "$(rbenv root)"/plugins/ruby-build && curl -LSs "https://github.com/rbenv/ruby-build/archive/refs/tags/v${RUBYBUILD_VERSION}.tar.gz" | tar -xz -C "$(rbenv root)"/plugins/ruby-build --strip-components 1 -f -

# check node version: # https://github.com/chatwoot/chatwoot/blob/v4.0.2/package.json#L144
# https://nodejs.org/dist/
ARG NODE_VERSION=23.6.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH=/usr/local/node-${NODE_VERSION}/bin:$PATH

# check pnpm version: # https://github.com/chatwoot/chatwoot/blob/v4.0.2/package.json#L145
RUN npm install -g pnpm@10.4.1

# renovate: datasource=github-releases depName=chatwoot/chatwoot versioning=semver extractVersion=^v(?<version>.+)$
ARG CHATWOOT_VERSION=4.0.3
RUN curl -LSs "https://github.com/chatwoot/chatwoot/archive/refs/tags/v${CHATWOOT_VERSION}.tar.gz" | tar -xz -C /app/code/ --strip-components 1 -f -

# install specific ruby version
RUN ruby_version=$(cat /app/code/.ruby-version) && \
    rbenv install ${ruby_version}

# not sure how to make this dynamic from above ruby-version
ENV PATH=/root/.rbenv/versions/3.3.3/bin:$PATH

RUN gem install bundler

ENV RAILS_ENV="production"
ENV EXECJS_RUNTIME="Disabled"
ENV BUNDLE_WITHOUT="development:test"
ENV RAILS_SERVE_STATIC_FILES="true"
ENV INSTALLATION_ENV="cloudron"
ENV NODE_OPTIONS="--max-old-space-size=4096"

RUN bundle install -j 4 -r 3 && \
    pnpm install && \
    SECRET_KEY_BASE=precompile_placeholder RAILS_LOG_TO_STDOUT=enabled bundle exec rake assets:precompile && \
    rm -rf spec node_modules tmp/cache

RUN ln -s /app/data/env /app/code/.env && \
    rm -rf /app/code/storage && ln -s /app/data/storage /app/code/storage && \
    rm -rf /app/code/tmp     && ln -s /tmp /app/code/tmp

# add supervisor configs
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/* /etc/supervisor/conf.d/

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
